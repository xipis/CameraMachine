cc.Class({
    extends: cc.Component,
    properties: {
        farCamera:  cc.Camera,   // 远景摄像机
        nearCamera: cc.Camera,   // 近景摄像机
        bg01: cc.Sprite,         
        bg02: cc.Sprite,
        nearbg01: cc.Sprite,  // 地板
        nearbg02: cc.Sprite,
    }, 
    start() {
        
    },
    onLoad() {
        this.farCount = 1;      //远计数
        this.nearCount = 1;     //进计数
        this.farSpeed = 30;     //远处速度
        this.nearSpeed = 300;   //近处的速度
    },
    update(dt) {  // 更新摄像机
        this.updateFarCamera(dt); //远相机
        this.updateNearCamera(dt);//近相机
    },
    updateFarCamera(dt) {
        this.farCamera.node.x += dt*this.farSpeed;
        let yu = this.farCount % 2;
        if (yu == 1) {
            if (this.farCamera.node.x > this.farCount * this.bg01.node.width) {
                this.bg01.node.x = (this.farCount+1) * this.bg01.node.width;
                this.farCount++;
            }
        } else {
            if (this.farCamera.node.x > this.farCount * this.bg01.node.width) {
                this.bg02.node.x = (this.farCount+1) * this.bg01.node.width;
                this.farCount++;
            }
        }
    },
    updateNearCamera(dt) {
        this.nearCamera.node.x += dt*this.nearSpeed;
        let yu = this.nearCount % 2;
        if (yu == 1) {
            if (this.nearCamera.node.x > this.nearCount * this.nearbg01.node.width) {
                this.nearbg01.node.x = (this.nearCount+1) * this.nearbg01.node.width;
                this.nearCount++;
            }
        } else {
            if (this.nearCamera.node.x > this.nearCount * this.nearbg02.node.width) {
                this.nearbg02.node.x = (this.nearCount+1) * this.nearbg02.node.width;
                this.nearCount++;
            }
        }
    }
});
